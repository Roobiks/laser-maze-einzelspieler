// array for mirror position
var arrayMirror = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];

// array for names
var arrayNames = ["Einstein"];

// for changing nickname
var saveBetween = ["empty"]

// for unblocking players in sequence
var unblock = 0;

// unblock playground
var unblockp;

var len;

// score
var score = 300;

// token
var token;

// join
var join;

// important mirrors turned
var important = 0;

// unimportant mirrors turned
var unimportant = 0;

// laser
var laser = 1;

// level
var level= 1;

// level increment
var levelIncrement =1;

// important mirrors to be turned level 1
var l1= 4;

// important mirrors to be turned level 2
var l2= 6;

// important mirrors to be turned level 3
var l3= 4;

// important mirrors to be turned level 4
var l4= 3;

function User(socket) {
	this.socket = socket;
	// assign a random number to User.
	this.id = "1" + Math.floor( Math.random() * 1000000000);
	
}

function Room() {
	this.users = [];
}

Room.prototype.addUser = function(user){
	len = this.users.length;
	this.handleOnUserMessage(user);
	
	
	// one player, block
	if (len == 0){console.log("One player here!");
	this.users.push(user);
	var room = this;
	
	// set avatar of first player
	user.id="Einstein";
	console.log(user.id);
	
	//send name to first player
	message = "Einstein";
	room.sendFirstNicks(message,user);
	
	message = "(Nicks) ";
	room.sendNicks(message);
	
	// unblock
	var msg="(Unblock)";
	room.sendUnblock(msg,user);
	console.log(score);
	// handle user closing
	user.socket.onclose = function(){
		console.log(user.id + " left.");
		// send final message
		message = "(Leave)";
		room.removeAllUsers(message,user);
		
		
		len=0;}
	} // end of if
	else{}

};

// sendNicks to all players
Room.prototype.sendNicks = function(message){
	for (var i=0, len=this.users.length; i<len; i++){
		var message2 = "(Nicks) " + arrayNames[0] + ", " + arrayNames[1] + ", ";
		this.users[i].socket.send(message2);
	}
};



// change nicks in array
Room.prototype.changeNicks = function(message,user) {
	
	cutmessage = message;
	// cut message to get new nickname
	var newnick = cutmessage.slice(13, cutmessage.length);

	// loop to find user
	for (var i=this.users.length; i>=0; i--){
		if (this.users[i] === user){
			saveBetween[0] = user.id;
			this.users[i].id = newnick;
			arrayNames[i] = newnick;
		}
	}
	
};


// send first nicks
Room.prototype.sendFirstNicks = function(message,user) {
	// loop to find user
	for (var i=this.users.length; i>=0; i--){
		if (this.users[i] === user){
			var message2 = "(FirstNick) " + message;
			this.users[i].socket.send(message2);
		}
	}
};


// unblock player
Room.prototype.sendUnblock = function(message,user){
	
		for (var i=0, len=this.users.length; i<len; i++){
		this.users[i].socket.send(message);
		
	}
	
};


// remove all players and send good bye message
Room.prototype.removeAllUsers = function(message,user) {
	
	message ="(Leave)";
	for (var i=0, len=this.users.length; i<len; i++){
		this.users[i].socket.send(message);
	}
	
	this.users.splice(0,1);
	
	// set back all the variables for new game
	arrayMirror=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
	score = 300;
	important=0;
	unimportant=0;
	laser =1;
	level =1;
	levelIncrement =1;
	
	
};



// new game message
Room.prototype.newGame = function(message,user){
	
	
	for (var i=0, len=this.users.length; i<len; i++){
		var message = "(newGame)";
		this.users[i].socket.send(message);
	}
	
	// reset all relevant variables for new game
	arrayMirror=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
	score = 300;
	important=0;
	unimportant=0;
	laser =1;
	level =1;
	levelIncrement =1;
	
};

// new game message
Room.prototype.back = function(message,user){
	
	// reset all relevant variables for new game
	arrayMirror=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
	score = 300;
	important=0;
	unimportant=0;
	laser =1;
	level =1;
	levelIncrement =1;
	
};

// top score message
Room.prototype.topScoreMessage = function(message,user){
	
	
	for (var i=0, len=this.users.length; i<len; i++){
		var message = "(topScore)" + level + score +"\n";
		this.users[i].socket.send(message);
	}


};

// token message for score entry
Room.prototype.token = function(message,user){
	
	
	// generate random token
	// without this token, score entries will be tripled
		token = "(token)1" + Math.floor( Math.random() * 1000000000);
	
	for (var i=0, len=this.users.length; i<len; i++){
		this.users[i].socket.send(token);
		
	}


};



// normal score message
Room.prototype.normalScoreMessage = function(message,user){
	
	
	for (var i=0, len=this.users.length; i<len; i++){
		var message = "(normalScore)" + level + score +"\n";
		this.users[i].socket.send(message);
	}
	
	
};


// failure message
Room.prototype.failureMessage = function(message,user){
	
	
	for (var i=0, len=this.users.length; i<len; i++){
		var message = "(failure)" + "\n";
		this.users[i].socket.send(message);
	}
	
	
};

// game over message
Room.prototype.GameOver = function(message,user){
	
	
	for (var i=0, len=this.users.length; i<len; i++){
		var message = "(GameOver)" + "\n";
		this.users[i].socket.send(message);
	}
	
	// reset all relevant variables for new game
	arrayMirror=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
	score = 300;
	important=0;
	unimportant=0;
	laser =1;
	level =1;
	levelIncrement =1;
	
	
};



// check incoming messages
Room.prototype.handleOnUserMessage = function(user) {
	var room = this;
	user.socket.on("message", function(message) {
		
		// change nickname
		var str = message.slice(0,12)
		if (str=="(changeNick)"){
			room.changeNicks(message,user)
			room.sendNicks(message)
			
		} 
		
		// laser for all levels
		
		if (message=="(laser)"){
			// level 1
			if (level==1){a=0; b=1; c=0; d=1; e=1; f=1; g=0; h=0; j=l1;} else{}
			// level 2
			if (level==2){a=1; b=0; c=1; d=1; e=1; f=1; g=0; h=1; j=l2;} else {}
			// level 3
			if (level==3){a=1; b=0; c=1; d=0; e=1; f=0; g=1; h=0; j=l3;} else {}
			// level 4
			if (level==4){a=0; b=0; c=1; d=1; e=1; f=0; g=0; h=0; j=l4;} else {}
			
			
			// if only important mirrors are turned and are in right angle and first laser activation, extra bonus
			if ((level==levelIncrement)&&(laser==1)&&(arrayMirror[0]==a)&&(arrayMirror[1]==b)&&(arrayMirror[2]==c)&&(arrayMirror[3]==d)&&(arrayMirror[4]==e)&&(arrayMirror[5]==f)&&(arrayMirror[6]==g)&&(arrayMirror[7]==h)&&(important==j)&&(unimportant==0)){
				//score
				score = score + 50;     
				// send token
				if (level==1){room.token();}
				// top score message
				message = "topScore";
				room.topScoreMessage(message,user);
				console.log(score);
				//reset
				important = 0;
				unimportant=0;
				laser= 1;
				levelIncrement = levelIncrement +1;
				level = level +1;
				arrayMirror = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
				
			// set sequence
			if (level==5){}
			else{}
			

				
				
			}
			// if all important mirrors are turned and laser activation, unimportant== 0, important !==0
			else if ((level==levelIncrement)&&(arrayMirror[0]==a)&&(arrayMirror[1]==b)&&(arrayMirror[2]==c)&&(arrayMirror[3]==d)&&(arrayMirror[4]==e)&&(arrayMirror[5]==f)&&(arrayMirror[6]==g)&&(arrayMirror[7]==h)&&(unimportant==0)&&(important!==0)){
				//score
				score = score + 20;
				// send token
				if (level==1){room.token();}
				// success message
				message = "normalScore";
				room.normalScoreMessage(message,user);
				console.log(score);
				
				important = 0;
				unimportant=0;
				laser= 1;
				levelIncrement = levelIncrement +1;
				level = level +1;
				arrayMirror = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
			
			// set sequence
			if (level==5){}
			else{}
			
			
			}
			// if all important mirrors are turned and laser activation, unimportant !==0, important !==0
			else if ((level==levelIncrement)&&(arrayMirror[0]==a)&&(arrayMirror[1]==b)&&(arrayMirror[2]==c)&&(arrayMirror[3]==d)&&(arrayMirror[4]==e)&&(arrayMirror[5]==f)&&(arrayMirror[6]==g)&&(arrayMirror[7]==h)&&(unimportant!==0)&&(important!==0)){
				score = score + 20;
				// send token
				if (level==1){room.token();}
				// success message
				message = "normalScore";
				room.normalScoreMessage(message,user);
				console.log(score);
				
				important = 0;
				unimportant=0;
				laser= 1;
				levelIncrement = levelIncrement +1;
				level = level +1;
				arrayMirror = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];

			
			}
			// if not all important mirrors are turned and laser activation
			else if (laser<=2) {
			   score = score - 40;
			   // failure message
			   message = "failure";
			   room.failureMessage(message,user);
			   console.log(score);

			   laser = laser +1;
			   
			}
			// Game Over
			else {
				// Game Over message
				message = "GameOver";
				room.GameOver(message);
				
			}
				

			}
			
			
		// back	
		if (message=="(back)"){	
		var msg="(back)";
		room.back(msg,user);

		
		}
		
		// new Game	
		if (message=="(newGame)"){	
		var msg="(newGame)";
		room.newGame(msg,user);

		
		}
			
		// all the mirrors	
		// mirror 1_0
		if (message=="mirror1_0"){
			arrayMirror[0]=0;

			// score
			var i;
			if (level==1){i=l1;}
			if (level==2){i=l2;}
			if (level==3){i=l3;}
			if (level==4){i=l4;}
			
			if (score <= 0){var message="GameOver"; room.GameOver(message);}
			else if (important>i){score = score - 20; important = important + 1;}
			else {important = important + 1;}
			console.log(score);
			}
		// mirror 1_1
		if (message=="mirror1_1"){
			arrayMirror[0]=1;

			// score
			var i;
			if (level==1){i=l1;}
			if (level==2){i=l2;}
			if (level==3){i=l3;}
			if (level==4){i=l4;}
			
			if (score <= 0){var message="GameOver"; room.GameOver(message);}
			else if (important>i){score = score - 20; important = important + 1;}
			else {important = important + 1;}
			console.log(score);
			}
			
		// mirror 2_0
		if (message=="mirror2_0"){
			arrayMirror[1]=0;
			
			// score
			var i;
			if (level==1){i=l1;}
			if (level==2){i=l2;}
			if (level==3){i=l3;}
			if (level==4){i=l4;}
			
			if (score <= 0){var message="GameOver"; room.GameOver(message);}
			else if (important>i){score = score - 20; important = important + 1;}
			else {important = important + 1;}
			console.log(score);
			}
		// mirror 2_1
		if (message=="mirror2_1"){
			arrayMirror[1]=1;
			
			// score
			var i;
			if (level==1){i=l1;}
			if (level==2){i=l2;}
			if (level==3){i=l3;}
			if (level==4){i=l4;}
			
			if (score <= 0){var message="GameOver"; room.GameOver(message);}
			else if (important>i){score = score - 20; important = important + 1;}
			else {important = important + 1;}
			console.log(score);
			}
			
		// mirror 3_0
		if (message=="mirror3_0"){
			arrayMirror[2]=0;
			
			// score
			var i;
			if (level==1){i=l1;}
			if (level==2){i=l2;}
			if (level==3){i=l3;}
			if (level==4){i=l4;}
			
			if (score <= 0){var message="GameOver"; room.GameOver(message);}
			else if (important>i){score = score - 20; important = important + 1;}
			else {important = important + 1;}
			console.log(score);
			}
		// mirror 3_1
		if (message=="mirror3_1"){
			arrayMirror[2]=1;
			
			// score
			var i;
			if (level==1){i=l1;}
			if (level==2){i=l2;}
			if (level==3){i=l3;}
			if (level==4){i=l4;}
			
			if (score <= 0){var message="GameOver"; room.GameOver(message);}
			else if (important>i){score = score - 20; important = important + 1;}
			else {important = important + 1;}
			console.log(score);
			}
		
		// mirror 4_0
		if (message=="mirror4_0"){
			arrayMirror[3]=0;
			
			// score
			var i;
			if (level==1){i=l1;}
			if (level==2){i=l2;}
			if (level==3){i=l3;}
			if (level==4){i=l4;}
			
			if (score <= 0){var message="GameOver"; room.GameOver(message);}
			else if (important>i){score = score - 20; important = important + 1;}
			else {important = important + 1;}
			console.log(score);
			}
		// mirror 4_1
		if (message=="mirror4_1"){
			arrayMirror[3]=1;
			
			// score
			var i;
			if (level==1){i=l1;}
			if (level==2){i=l2;}
			if (level==3){i=l3;}
			if (level==4){i=l4;}
			
			if (score <= 0){var message="GameOver"; room.GameOver(message);}
			else if (important>i){score = score - 20; important = important + 1;}
			else {important = important + 1;}
			console.log(score);
			}
			
		// mirror 5_0
		if (message=="mirror5_0"){
			arrayMirror[4]=0;

			// score
			var i;
			if (level==1){i=l1;}
			if (level==2){i=l2;}
			if (level==3){i=l3;}
			if (level==4){i=l4;}
			
			if (score <= 0){var message="GameOver"; room.GameOver(message);}
			else if (important>i){score = score - 20; important = important + 1;}
			else {important = important + 1;}
			console.log(score);
			}
		// mirror 5_1
		if (message=="mirror5_1"){
			arrayMirror[4]=1;
			
			// score
			var i;
			if (level==1){i=l1;}
			if (level==2){i=l2;}
			if (level==3){i=l3;}
			if (level==4){i=l4;}
			
			if (score <= 0){var message="GameOver"; room.GameOver(message);}
			else if (important>i){score = score - 20; important = important + 1;}
			else {important = important + 1;}
			console.log(score);
			}
			
		// mirror 6_0
		if (message=="mirror6_0"){
			arrayMirror[5]=0;
			
			// score
			var i;
			if (level==1){i=l1;}
			if (level==2){i=l2;}
			if (level==3){i=l3;}
			if (level==4){i=l4;}
			
			if (score <= 0){var message="GameOver"; room.GameOver(message);}
			else if (important>i){score = score - 20; important = important + 1;}
			else {important = important + 1;}
			console.log(score);
			}
		// mirror 6_1
		if (message=="mirror6_1"){
			arrayMirror[5]=1;
			
			// score
			var i;
			if (level==1){i=l1;}
			if (level==2){i=l2;}
			if (level==3){i=l3;}
			if (level==4){i=l4;}
			
			if (score <= 0){var message="GameOver"; room.GameOver(message);}
			else if (important>i){score = score - 20; important = important + 1;}
			else {important = important + 1;}
			console.log(score);
			}
			
		// mirror 7_0
		if (message=="mirror7_0"){
			arrayMirror[6]=0;
			
			// score
			var i;
			if (level==1){i=l1;}
			if (level==2){i=l2;}
			if (level==3){i=l3;}
			if (level==4){i=l4;}
			
			if (score <= 0){var message="GameOver"; room.GameOver(message);}
			else if (important>i){score = score - 20; important = important + 1;}
			else {important = important + 1;}
			console.log(score);
			}
		// mirror 7_1
		if (message=="mirror7_1"){
			arrayMirror[6]=1;
			
			// score
			var i;
			if (level==1){i=l1;}
			if (level==2){i=l2;}
			if (level==3){i=l3;}
			if (level==4){i=l4;}
			
			if (score <= 0){var message="GameOver"; room.GameOver(message);}
			else if (important>i){score = score - 20; important = important + 1;}
			else {important = important + 1;}
			console.log(score);
			}

		// mirror 8_0
		if (message=="mirror8_0"){
			arrayMirror[7]=0;
			
			// score
			var i;
			if (level==1){i=l1;}
			if (level==2){i=l2;}
			if (level==3){i=l3;}
			if (level==4){i=l4;}
			
			if (score <= 0){var message="GameOver"; room.GameOver(message);}
			else if (important>i){score = score - 20; important = important + 1;}
			else {important = important + 1;}
			console.log(score);
			}
		// mirror 8_1
		if (message=="mirror8_1"){
			arrayMirror[7]=1;
			
			// score
			var i;
			if (level==1){i=l1;}
			if (level==2){i=l2;}
			if (level==3){i=l3;}
			if (level==4){i=l4;}
			
			if (score <= 0){var message="GameOver"; room.GameOver(message);}
			else if (important>i){score = score - 20; important = important + 1;}
			else {important = important + 1;}
			console.log(score);
			}
		// mirror 9_0
		if (message=="mirror9_0"){
			arrayMirror[8]=0;

			if (score <= 0){var message="GameOver"; room.GameOver(message);}
			else {score = score - 30; unimportant = unimportant + 1;}
			console.log(score);
			}
		// mirror 9_1
		if (message=="mirror9_1"){
			arrayMirror[8]=1;

			if (score <= 0){var message="GameOver"; room.GameOver(message);}
			else {score = score - 30; unimportant = unimportant + 1;}
			console.log(score);
			}
		// mirror 10_0
		if (message=="mirror10_0"){
			arrayMirror[9]=0;

			if (score <= 0){var message="GameOver"; room.GameOver(message);}
			else {score = score - 30; unimportant = unimportant + 1;}
			console.log(score);
			}
		// mirror 10_1
		if (message=="mirror10_1"){
			arrayMirror[9]=1;

			if (score <= 0){var message="GameOver"; room.GameOver(message);}
			else {score = score - 30; unimportant = unimportant + 1;}
			console.log(score);
			}
		// mirror 11_0
		if (message=="mirror11_0"){
			arrayMirror[10]=0;

			if (score <= 0){var message="GameOver"; room.GameOver(message);}
			else {score = score - 30; unimportant = unimportant + 1;}
			console.log(score);
			}
		// mirror 11_1
		if (message=="mirror11_1"){
			arrayMirror[10]=1;

		    if (score <= 0){var message="GameOver"; room.GameOver(message);}
			else {score = score - 30; unimportant = unimportant + 1;}
			console.log(score);
			}	
		// mirror 12_0
		if (message=="mirror12_0"){
			arrayMirror[11]=0;

			if (score <= 0){var message="GameOver"; room.GameOver(message);}
			else {score = score - 30; unimportant = unimportant + 1;}
			console.log(score);
			}
		// mirror 12_1
		if (message=="mirror12_1"){
			arrayMirror[11]=1;

			if (score <= 0){var message="GameOver"; room.GameOver(message);}
			else {score = score - 30; unimportant = unimportant + 1;}
			console.log(score);
			}
		// mirror 13_0
		if (message=="mirror13_0"){
			arrayMirror[12]=0;

			if (score <= 0){var message="GameOver"; room.GameOver(message);}
			else {score = score - 30; unimportant = unimportant + 1;}
			console.log(score);
			}
		// mirror 13_1
		if (message=="mirror13_1"){
			arrayMirror[12]=1;

			if (score <= 0){var message="GameOver"; room.GameOver(message);}
			else {score = score - 30; unimportant = unimportant + 1;}
			console.log(score);
			}
		// mirror 14_0
		if (message=="mirror14_0"){
			arrayMirror[13]=0;

			if (score <= 0){var message="GameOver"; room.GameOver(message);}
			else {score = score - 30; unimportant = unimportant + 1;}
			console.log(score);
			}
		// mirror 14_1
		if (message=="mirror14_1"){
			arrayMirror[13]=1;

			if (score <= 0){var message="GameOver"; room.GameOver(message);}
			else {score = score - 30; unimportant = unimportant + 1;}
			console.log(score);
			}	
		// mirror 15_0
		if (message=="mirror15_0"){
			arrayMirror[14]=0;

			if (score <= 0){var message="GameOver"; room.GameOver(message);}
			else {score = score - 30; unimportant = unimportant + 1;}
			console.log(score);
			}
		// mirror 15_1
		if (message=="mirror15_1"){
			arrayMirror[14]=1;

			if (score <= 0){var message="GameOver"; room.GameOver(message);}
			else {score = score - 30; unimportant = unimportant + 1;}
			console.log(score);
			}				
					
		
	});
	
	
};



module.exports.User = User;
module.exports.Room = Room;
